package com.flight.flight.infrastructure.repository;

import com.flight.flight.domain.entity.AirportEntity;
import com.flight.flight.domain.repository.AirportRepository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public interface AirportJpaRepository extends JpaRepository<AirportEntity, Long>, AirportRepository {
    List<AirportEntity> findByName(String name);

    List<AirportEntity> findByShortName(String shortName);

    List<AirportEntity> findByCity(String city);

    List<AirportEntity> findByCountry(String country);

}
