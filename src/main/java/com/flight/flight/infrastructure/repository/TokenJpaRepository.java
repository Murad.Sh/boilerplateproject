package com.flight.flight.infrastructure.repository;

import com.flight.flight.domain.entity.token.Token;
import com.flight.flight.domain.repository.TokenRepository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TokenJpaRepository extends JpaRepository<Token, Long>, TokenRepository {

}
