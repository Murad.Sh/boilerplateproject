package com.flight.flight.infrastructure.repository;

import com.flight.flight.domain.entity.PassengerEntity;
import com.flight.flight.domain.repository.PassengerRepository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public interface PassengerJpaRepository extends JpaRepository<PassengerEntity, Long>, PassengerRepository {

    List<PassengerEntity> findByName(String name);

    List<PassengerEntity> findBySurname(String surname);

    List<PassengerEntity> findByEmail(String email);

    List<PassengerEntity> findByCountry(String country);

    List<PassengerEntity> findByPassportNumber(String passportNumber);
}
