package com.flight.flight.infrastructure.repository;

import com.flight.flight.domain.entity.PlaneEntity;
import com.flight.flight.domain.repository.PlaneRepository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public interface PlaneJpaRepository extends JpaRepository<PlaneEntity, Long>, PlaneRepository {
    List<PlaneEntity> findByName(String name);

    List<PlaneEntity> findByNumberOfSeats( Integer numberOfSeats);

    List<PlaneEntity> findByNameOfCompany( String nameOfCompany);
}
