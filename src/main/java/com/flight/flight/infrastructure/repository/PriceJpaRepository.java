package com.flight.flight.infrastructure.repository;

import com.flight.flight.domain.entity.CabinClass.CabinClassType;
import com.flight.flight.domain.entity.PriceEntity;
import com.flight.flight.domain.repository.PriceRepository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public interface PriceJpaRepository extends JpaRepository<PriceEntity, Long>, PriceRepository {
    List<PriceEntity> findByCabinClass(CabinClassType cabinClass);

    List<PriceEntity> findByMinMultiplier(Double minMultiplier);

    List<PriceEntity> findByMaxMultiplier(Double maxMultiplier);
}
