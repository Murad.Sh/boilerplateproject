package com.flight.flight.infrastructure.repository;

import com.flight.flight.domain.entity.AirportEntity;
import com.flight.flight.domain.entity.FlightEntity;
import com.flight.flight.domain.entity.PlaneEntity;
import com.flight.flight.domain.entity.PriceEntity;
import com.flight.flight.domain.repository.FlightRepository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import java.util.Date;
import java.util.List;

@Repository
public interface FlightJpaRepository extends JpaRepository<FlightEntity, Long>, FlightRepository {
    List<FlightEntity> findByDepartureAirport(AirportEntity departureAirport);

    List<FlightEntity> findByArrivalAirport(AirportEntity arrivalAirport);

    List<FlightEntity> findByDepartureDate(Date departureDate);

    List<FlightEntity> findByArrivalDate(Date departureDate);

    List<FlightEntity> findByDistance(Integer distance);

    List<FlightEntity> findByFlightTime(Integer flightTime);

    List<FlightEntity> findByPlaneId(PlaneEntity planeId);

    List<FlightEntity> findByPriceId(PriceEntity priceId);


}
