package com.flight.flight.infrastructure.repository;

import com.flight.flight.domain.entity.user.User;
import com.flight.flight.domain.repository.UserRepository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserJpaRepository extends JpaRepository<User, Integer>, UserRepository {
}