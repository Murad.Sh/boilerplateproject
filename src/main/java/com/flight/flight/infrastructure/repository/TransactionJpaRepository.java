package com.flight.flight.infrastructure.repository;

import com.flight.flight.domain.entity.Status.Status;
import com.flight.flight.domain.entity.TransactionEntity;
import com.flight.flight.domain.repository.TransactionRepository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import java.util.Date;
import java.util.List;

@Repository
public interface TransactionJpaRepository extends JpaRepository<TransactionEntity, Long>, TransactionRepository {
    List<TransactionEntity> findByRrn(String rrn);

    List<TransactionEntity> findByInvoiceCode(String invoiceCode);

    List<TransactionEntity> findByPrice(Double price);

    List<TransactionEntity> findByPaymentDate(Date paymentDate);

    List<TransactionEntity> findByStatus(Status status);

    List<TransactionEntity> findByCreatedAt(Date createdAt);

    List<TransactionEntity> findByUpdatedAt(Date updatedAt);

    List<TransactionEntity> findByFlightId(Integer flightId);

    List<TransactionEntity> findByPassengerId(Integer passengerId);
}
