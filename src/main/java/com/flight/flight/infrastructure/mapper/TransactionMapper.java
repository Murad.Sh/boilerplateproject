package com.flight.flight.infrastructure.mapper;

import com.flight.flight.application.dto.TransactionDTO;
import com.flight.flight.domain.entity.TransactionEntity;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface TransactionMapper {
    static TransactionDTO mapTransactionEntityToTransactionDTO(TransactionEntity entity) {
        if (entity == null) {
            return null;
        }

        TransactionDTO dto = new TransactionDTO();
        dto.setId(entity.getId());
        dto.setRrn(entity.getRrn());
        dto.setInvoiceCode(entity.getInvoiceCode());
        dto.setPrice(entity.getPrice());
        dto.setPaymentDate(entity.getPaymentDate());
        dto.setStatus(entity.getStatus());
        dto.setCreatedAt(entity.getCreatedAt());
        dto.setUpdatedAt(entity.getUpdatedAt());
        dto.setFlightId(entity.getFlightId());
        dto.setPassengerId(entity.getPassengerId());

        return dto;
    }

    static TransactionEntity mapTransactionDTOToTransactionEntity(TransactionDTO dto) {
        if (dto == null) {
            return null;
        }

        TransactionEntity entity = new TransactionEntity();

        entity.setId(dto.getId());
        entity.setRrn(dto.getRrn());
        entity.setInvoiceCode(dto.getInvoiceCode());
        entity.setPrice(dto.getPrice());
        entity.setPaymentDate(dto.getPaymentDate());
        entity.setStatus(dto.getStatus());
        entity.setCreatedAt(dto.getCreatedAt());
        entity.setUpdatedAt(dto.getUpdatedAt());
        entity.setFlightId(dto.getFlightId());
        entity.setPassengerId(dto.getPassengerId());


        return entity;
    }
}
