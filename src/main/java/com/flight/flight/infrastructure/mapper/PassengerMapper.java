package com.flight.flight.infrastructure.mapper;

import com.flight.flight.application.dto.PassengerDTO;
import com.flight.flight.domain.entity.PassengerEntity;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface PassengerMapper {
    static PassengerDTO mapPassengerEntityToPassengerDTO(PassengerEntity entity) {
        if (entity == null) {
            return null;
        }

        PassengerDTO dto = new PassengerDTO();
        dto.setId(entity.getId());
        dto.setName(entity.getName());
        dto.setSurname(entity.getSurname());
        dto.setEmail(entity.getEmail());
        dto.setCountry(entity.getCountry());
        dto.setPassportNumber(entity.getPassportNumber());

        return dto;
    }

    static PassengerEntity mapPassengerDTOToPassengerEntity(PassengerDTO dto) {
        if (dto == null) {
            return null;
        }

        PassengerEntity entity = new PassengerEntity();

        entity.setId(dto.getId());
        entity.setName(dto.getName());
        entity.setSurname(dto.getSurname());
        entity.setEmail(dto.getEmail());
        entity.setCountry(dto.getCountry());
        entity.setPassportNumber(dto.getPassportNumber());

        return entity;
    }
}
