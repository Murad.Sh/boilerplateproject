package com.flight.flight.infrastructure.mapper;

import com.flight.flight.domain.entity.user.User;
import com.flight.flight.application.dto.UserDTO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring")
public interface UserMapper {

    UserMapper INSTANCE = Mappers.getMapper(UserMapper.class);

    UserDTO mapUserToUserDto(User user);

    User mapUserDtoToUser(UserDTO userDto);
}
