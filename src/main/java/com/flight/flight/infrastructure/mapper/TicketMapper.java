package com.flight.flight.infrastructure.mapper;

import com.flight.flight.application.dto.TicketDTO;
import com.flight.flight.domain.entity.TicketEntity;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface TicketMapper {
    static TicketDTO mapTicketEntityToTicketDTO(TicketEntity entity) {
        if (entity == null) {
            return null;
        }

        TicketDTO dto = new TicketDTO();
        dto.setId(entity.getId());
        dto.setFlight(entity.getFlight());
        dto.setPassenger(entity.getPassenger());
        dto.setTransaction(entity.getTransaction());

        return dto;
    }

    static TicketEntity mapTicketDTOToTicketEntity(TicketDTO dto) {
        if (dto == null) {
            return null;
        }

        TicketEntity entity = new TicketEntity();

        entity.setId(dto.getId());
        entity.setFlight(dto.getFlight());
        entity.setPassenger(dto.getPassenger());
        entity.setTransaction(dto.getTransaction());


        return entity;
    }
}
