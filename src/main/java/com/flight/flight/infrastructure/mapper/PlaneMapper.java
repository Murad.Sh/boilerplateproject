package com.flight.flight.infrastructure.mapper;

import com.flight.flight.application.dto.PlaneDTO;
import com.flight.flight.domain.entity.PlaneEntity;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface PlaneMapper {
    static PlaneDTO mapPlaneEntityToPlaneDTO(PlaneEntity entity) {
        if (entity == null) {
            return null;
        }

        PlaneDTO dto = new PlaneDTO();
        dto.setId(entity.getId());
        dto.setName(entity.getName());
        dto.setNumberOfSeats(entity.getNumberOfSeats());
        dto.setNameOfCompany(entity.getNameOfCompany());

        return dto;
    }

    static PlaneEntity mapPlaneDTOToPlaneEntity(PlaneDTO dto) {
        if (dto == null) {
            return null;
        }

        PlaneEntity entity = new PlaneEntity();

        dto.setId(entity.getId());
        dto.setName(entity.getName());
        dto.setNumberOfSeats(entity.getNumberOfSeats());
        dto.setNameOfCompany(entity.getNameOfCompany());

        return entity;
    }
}
