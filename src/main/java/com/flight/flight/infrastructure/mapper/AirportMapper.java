package com.flight.flight.infrastructure.mapper;

import com.flight.flight.application.dto.AirportDTO;
import com.flight.flight.domain.entity.AirportEntity;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface AirportMapper {
     static AirportDTO mapAirportEntityToAirportDTO(AirportEntity entity) {
        if (entity == null) {
            return null;
        }

        AirportDTO dto = new AirportDTO();
        dto.setId(entity.getId());
        dto.setName(entity.getName());
        dto.setShortName(entity.getShortName());
        dto.setCity(entity.getCity());
        dto.setCountry(entity.getCountry());

        return dto;
    }

     static AirportEntity mapAirportDTOToAirportEntity(AirportDTO dto) {
        if (dto == null) {
            return null;
        }

        AirportEntity entity = new AirportEntity();
        entity.setId(dto.getId());
        entity.setName(dto.getName());
        entity.setShortName(dto.getShortName());
        entity.setCity(dto.getCity());
        entity.setCountry(dto.getCountry());

        return entity;
    }
}
