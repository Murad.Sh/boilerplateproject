package com.flight.flight.infrastructure.mapper;

import com.flight.flight.application.dto.FlightDTO;
import com.flight.flight.domain.entity.FlightEntity;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface FlightMapper {
    static FlightDTO mapFlightEntityToFlightDTO(FlightEntity entity) {
        if (entity == null) {
            return null;
        }

        FlightDTO dto = new FlightDTO();
        dto.setId(entity.getId());
        dto.setDepartureAirport(entity.getDepartureAirport());
        dto.setArrivalAirport(entity.getArrivalAirport());
        dto.setArrivalDate(entity.getArrivalDate());
        dto.setDepartureDate(entity.getDepartureDate());
        dto.setDistance(entity.getDistance());
        dto.setFlightTime(entity.getFlightTime());
        dto.setPlaneId(entity.getPlaneId());
        dto.setPriceId(entity.getPriceId());

        return dto;
    }

    static FlightEntity mapFlightDTOToFlightEntity(FlightDTO dto) {
        if (dto == null) {
            return null;
        }

        FlightEntity entity = new FlightEntity();

        entity.setId(dto.getId());
        entity.setDepartureAirport(dto.getDepartureAirport());
        entity.setArrivalAirport(dto.getArrivalAirport());
        entity.setArrivalDate(dto.getArrivalDate());
        entity.setDepartureDate(dto.getDepartureDate());
        entity.setDistance(dto.getDistance());
        entity.setFlightTime(dto.getFlightTime());
        entity.setPlaneId(dto.getPlaneId());
        entity.setPriceId(dto.getPriceId());


        return entity;
    }
}
