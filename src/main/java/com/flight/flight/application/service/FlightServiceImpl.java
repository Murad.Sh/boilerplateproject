package com.flight.flight.application.service;

import com.flight.flight.application.dto.FlightDTO;
import com.flight.flight.domain.entity.AirportEntity;
import com.flight.flight.domain.entity.FlightEntity;
import com.flight.flight.domain.entity.PlaneEntity;
import com.flight.flight.domain.entity.PriceEntity;
import com.flight.flight.domain.repository.AirportRepository;
import com.flight.flight.domain.repository.FlightRepository;
import com.flight.flight.domain.repository.PlaneRepository;
import com.flight.flight.domain.repository.PriceRepository;
import com.flight.flight.domain.service.FlightService;
import com.flight.flight.presentation.presenter.flight.CreateFlightRequest;
import jakarta.persistence.EntityNotFoundException;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class FlightServiceImpl implements FlightService {
    @Autowired
    private FlightRepository flightRepository;

    @Autowired
    private AirportRepository airportRepository;

    @Autowired
    private PlaneRepository planeRepository;

    @Autowired
    private PriceRepository priceRepository;

    @SneakyThrows
    public Optional<FlightDTO> findById(Long id) {

        return flightRepository.findById(id)
                .map(flight -> FlightDTO.builder()
                        .id(flight.getId())
                        .departureAirport(flight.getDepartureAirport())
                        .arrivalAirport(flight.getArrivalAirport())
                        .departureDate(flight.getDepartureDate())
                        .arrivalDate(flight.getArrivalDate())
                        .distance(flight.getDistance())
                        .flightTime(flight.getFlightTime())
                        .planeId(flight.getPlaneId())
                        .priceId(flight.getPriceId())
                        .build());
    }

    @SneakyThrows
    public List<FlightDTO> findByDepartureAirport(AirportEntity departureAirport) {
        List<FlightEntity> flights = flightRepository.findByDepartureAirport(departureAirport);

        return flights.stream()
                .map(FlightEntity -> FlightDTO.builder()
                        .id(FlightEntity.getId())
                        .departureAirport(FlightEntity.getDepartureAirport())
                        .arrivalAirport(FlightEntity.getArrivalAirport())
                        .departureDate(FlightEntity.getDepartureDate())
                        .arrivalDate(FlightEntity.getArrivalDate())
                        .distance(FlightEntity.getDistance())
                        .flightTime(FlightEntity.getFlightTime())
                        .planeId(FlightEntity.getPlaneId())
                        .priceId(FlightEntity.getPriceId())
                        .build())
                .collect(Collectors.toList());
    }

    @SneakyThrows
    public List<FlightDTO> findByArrivalAirport(AirportEntity arrivalAirport) {
        List<FlightEntity> flights = flightRepository.findByArrivalAirport(arrivalAirport);

        return flights.stream()
                .map(FlightEntity -> FlightDTO.builder()
                        .id(FlightEntity.getId())
                        .departureAirport(FlightEntity.getDepartureAirport())
                        .arrivalAirport(FlightEntity.getArrivalAirport())
                        .departureDate(FlightEntity.getDepartureDate())
                        .arrivalDate(FlightEntity.getArrivalDate())
                        .distance(FlightEntity.getDistance())
                        .flightTime(FlightEntity.getFlightTime())
                        .planeId(FlightEntity.getPlaneId())
                        .priceId(FlightEntity.getPriceId())
                        .build())
                .collect(Collectors.toList());
    }

    @SneakyThrows
    public List<FlightDTO> findByDepartureDate(Date departureDate) {
        List<FlightEntity> flights = flightRepository.findByDepartureDate(departureDate);

        return flights.stream()
                .map(FlightEntity -> FlightDTO.builder()
                        .id(FlightEntity.getId())
                        .departureAirport(FlightEntity.getDepartureAirport())
                        .arrivalAirport(FlightEntity.getArrivalAirport())
                        .departureDate(FlightEntity.getDepartureDate())
                        .arrivalDate(FlightEntity.getArrivalDate())
                        .distance(FlightEntity.getDistance())
                        .flightTime(FlightEntity.getFlightTime())
                        .planeId(FlightEntity.getPlaneId())
                        .priceId(FlightEntity.getPriceId())
                        .build())
                .collect(Collectors.toList());
    }

    @SneakyThrows
    public List<FlightDTO> findByArrivalDate(Date arrivalDate) {
        List<FlightEntity> flights = flightRepository.findByArrivalDate(arrivalDate);

        return flights.stream()
                .map(FlightEntity -> FlightDTO.builder()
                        .id(FlightEntity.getId())
                        .departureAirport(FlightEntity.getDepartureAirport())
                        .arrivalAirport(FlightEntity.getArrivalAirport())
                        .departureDate(FlightEntity.getDepartureDate())
                        .arrivalDate(FlightEntity.getArrivalDate())
                        .distance(FlightEntity.getDistance())
                        .flightTime(FlightEntity.getFlightTime())
                        .planeId(FlightEntity.getPlaneId())
                        .priceId(FlightEntity.getPriceId())
                        .build())
                .collect(Collectors.toList());
    }

    @SneakyThrows
    public List<FlightDTO> findByDistance(Integer distance) {
        List<FlightEntity> flights = flightRepository.findByDistance(distance);

        return flights.stream()
                .map(FlightEntity -> FlightDTO.builder()
                        .id(FlightEntity.getId())
                        .departureAirport(FlightEntity.getDepartureAirport())
                        .arrivalAirport(FlightEntity.getArrivalAirport())
                        .departureDate(FlightEntity.getDepartureDate())
                        .arrivalDate(FlightEntity.getArrivalDate())
                        .distance(FlightEntity.getDistance())
                        .flightTime(FlightEntity.getFlightTime())
                        .planeId(FlightEntity.getPlaneId())
                        .priceId(FlightEntity.getPriceId())
                        .build())
                .collect(Collectors.toList());
    }

    @SneakyThrows
    public List<FlightDTO> findByFlightTime(Integer flightTime) {
        List<FlightEntity> flights = flightRepository.findByFlightTime(flightTime);

        return flights.stream()
                .map(FlightEntity -> FlightDTO.builder()
                        .id(FlightEntity.getId())
                        .departureAirport(FlightEntity.getDepartureAirport())
                        .arrivalAirport(FlightEntity.getArrivalAirport())
                        .departureDate(FlightEntity.getDepartureDate())
                        .arrivalDate(FlightEntity.getArrivalDate())
                        .distance(FlightEntity.getDistance())
                        .flightTime(FlightEntity.getFlightTime())
                        .planeId(FlightEntity.getPlaneId())
                        .priceId(FlightEntity.getPriceId())
                        .build())
                .collect(Collectors.toList());
    }

    @SneakyThrows
    public List<FlightDTO> findByPlaneId(PlaneEntity planeId) {
        List<FlightEntity> flights = flightRepository.findByPlaneId(planeId);

        return flights.stream()
                .map(FlightEntity -> FlightDTO.builder()
                        .id(FlightEntity.getId())
                        .departureAirport(FlightEntity.getDepartureAirport())
                        .arrivalAirport(FlightEntity.getArrivalAirport())
                        .departureDate(FlightEntity.getDepartureDate())
                        .arrivalDate(FlightEntity.getArrivalDate())
                        .distance(FlightEntity.getDistance())
                        .flightTime(FlightEntity.getFlightTime())
                        .planeId(FlightEntity.getPlaneId())
                        .priceId(FlightEntity.getPriceId())
                        .build())
                .collect(Collectors.toList());
    }

    @SneakyThrows
    public List<FlightDTO> findByPriceId(PriceEntity priceId) {
        List<FlightEntity> flights = flightRepository.findByPriceId(priceId);

        return flights.stream()
                .map(FlightEntity -> FlightDTO.builder()
                        .id(FlightEntity.getId())
                        .departureAirport(FlightEntity.getDepartureAirport())
                        .arrivalAirport(FlightEntity.getArrivalAirport())
                        .departureDate(FlightEntity.getDepartureDate())
                        .arrivalDate(FlightEntity.getArrivalDate())
                        .distance(FlightEntity.getDistance())
                        .flightTime(FlightEntity.getFlightTime())
                        .planeId(FlightEntity.getPlaneId())
                        .priceId(FlightEntity.getPriceId())
                        .build())
                .collect(Collectors.toList());
    }

    @SneakyThrows
    public List<FlightDTO> findAll() {
        return flightRepository.findAll().stream()
                .map(flightEntity -> FlightDTO.builder()
                        .id(flightEntity.getId())
                        .departureAirport(flightEntity.getDepartureAirport())
                        .arrivalAirport(flightEntity.getArrivalAirport())
                        .departureDate(flightEntity.getDepartureDate())
                        .arrivalDate(flightEntity.getArrivalDate())
                        .distance(flightEntity.getDistance())
                        .flightTime(flightEntity.getFlightTime())
                        .planeId(flightEntity.getPlaneId())
                        .priceId(flightEntity.getPriceId())
                        .build())
                .collect(Collectors.toList());
    }

    @SneakyThrows
    public void deleteById(Long id) {
        flightRepository.deleteById(id);
    }

    @SneakyThrows
    public FlightDTO save(CreateFlightRequest createFlightRequest) {
        AirportEntity arrivalAirport = airportRepository.findById(createFlightRequest.getArrivalAirportId())
                .orElseThrow(() -> new EntityNotFoundException("Arrival airport is not found" + createFlightRequest.getArrivalAirportId()));
        AirportEntity departureAirport = airportRepository.findById(createFlightRequest.getArrivalAirportId())
                .orElseThrow(() -> new EntityNotFoundException("Arrival airport is not found" + createFlightRequest.getArrivalAirportId()));

        PlaneEntity planeEntity = planeRepository.findById(createFlightRequest.getPlaneId())
                .orElseThrow(() -> new EntityNotFoundException("Plane not found: " + createFlightRequest.getPlaneId()));

        PriceEntity priceEntity = priceRepository.findById(createFlightRequest.getPriceId())
                .orElseThrow(() -> new EntityNotFoundException("Plane not found: " + createFlightRequest.getPriceId()));

        FlightEntity flight = FlightEntity.builder()
                .departureAirport(departureAirport)
                .arrivalAirport(arrivalAirport)
                .departureDate(createFlightRequest.getDepartureDate())
                .arrivalDate(createFlightRequest.getArrivalDate())
                .distance(createFlightRequest.getDistance())
                .flightTime(createFlightRequest.getFlightTime())
                .planeId(planeEntity)
                .priceId(priceEntity)
                .build();

        flight = flightRepository.save(flight);

        return FlightDTO.builder()
                .id(flight.getId())
                .departureAirport(flight.getDepartureAirport())
                .arrivalAirport(flight.getArrivalAirport())
                .departureDate(flight.getDepartureDate())
                .arrivalDate(flight.getArrivalDate())
                .distance(flight.getDistance())
                .flightTime(flight.getFlightTime())
                .planeId(flight.getPlaneId())
                .priceId(flight.getPriceId())
                .build();
    }

    public FlightDTO update(Long id, FlightDTO flightDTO) throws EntityNotFoundException {
        FlightEntity flightEntity = flightRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("Flight is not found" + id));

        flightEntity.setDepartureAirport(flightDTO.getDepartureAirport());
        flightEntity.setArrivalAirport(flightDTO.getArrivalAirport());
        flightEntity.setDepartureDate(flightDTO.getDepartureDate());
        flightEntity.setArrivalDate(flightDTO.getArrivalDate());
        flightEntity.setDistance(flightDTO.getDistance());
        flightEntity.setFlightTime(flightDTO.getFlightTime());
        flightEntity.setPlaneId(flightDTO.getPlaneId());
        flightEntity.setPriceId(flightDTO.getPriceId());

        FlightEntity updatedFlight = flightRepository.save(flightEntity);

        return FlightDTO.builder()
                .id(updatedFlight.getId())
                .departureAirport(updatedFlight.getDepartureAirport())
                .arrivalAirport(updatedFlight.getArrivalAirport())
                .departureDate(updatedFlight.getDepartureDate())
                .arrivalDate(updatedFlight.getArrivalDate())
                .distance(updatedFlight.getDistance())
                .flightTime(updatedFlight.getFlightTime())
                .planeId(updatedFlight.getPlaneId())
                .priceId(updatedFlight.getPriceId())
                .build();

    }

    @SneakyThrows
    public boolean existsById(Long id) {
        return flightRepository.existsById(id);
    }
}
