package com.flight.flight.application.service;
import com.flight.flight.application.dto.AirportDTO;
import com.flight.flight.domain.entity.AirportEntity;
import com.flight.flight.domain.repository.AirportRepository;
import com.flight.flight.domain.service.AirportService;
import jakarta.persistence.EntityNotFoundException;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class AirportServiceImpl implements AirportService {
    @Autowired
    private AirportRepository airportRepository;

    @SneakyThrows
    public Optional<AirportDTO> findById(Long id) {

        return airportRepository.findById(id)
                .map(airport -> AirportDTO.builder()
                        .id(airport.getId())
                        .name(airport.getName())
                        .shortName(airport.getShortName())
                        .city(airport.getCity())
                        .country(airport.getCountry())
                        .build());
    }

    @SneakyThrows
    public List<AirportDTO> findByName(String name) {
        List<AirportEntity> airport = airportRepository.findByName(name);

        return airport.stream()
                .map(AirportEntity -> AirportDTO.builder()
                        .id(AirportEntity.getId())
                        .name(AirportEntity.getName())
                        .shortName(AirportEntity.getShortName())
                        .city(AirportEntity.getCity())
                        .country(AirportEntity.getCountry())
                        .build())
                .collect(Collectors.toList());
    }

    @SneakyThrows
    public List<AirportDTO> findByShortName(String shortName) {
        List<AirportEntity> airport = airportRepository.findByShortName(shortName);

        return airport.stream()
                .map(AirportEntity -> AirportDTO.builder()
                        .id(AirportEntity.getId())
                        .name(AirportEntity.getName())
                        .shortName(AirportEntity.getShortName())
                        .city(AirportEntity.getCity())
                        .country(AirportEntity.getCountry())
                        .build())
                .collect(Collectors.toList());
    }

    @SneakyThrows
    public List<AirportDTO> findByCity(String city) {
        List<AirportEntity> airport = airportRepository.findByCity(city);

        return airport.stream()
                .map(AirportEntity -> AirportDTO.builder()
                        .id(AirportEntity.getId())
                        .name(AirportEntity.getName())
                        .shortName(AirportEntity.getShortName())
                        .city(AirportEntity.getCity())
                        .country(AirportEntity.getCountry())
                        .build())
                .collect(Collectors.toList());
    }

    @SneakyThrows
    public List<AirportDTO> findByCountry(String country) {
        List<AirportEntity> airport = airportRepository.findByCountry(country);

        return airport.stream()
                .map(AirportEntity -> AirportDTO.builder()
                        .id(AirportEntity.getId())
                        .name(AirportEntity.getName())
                        .shortName(AirportEntity.getShortName())
                        .city(AirportEntity.getCity())
                        .country(AirportEntity.getCountry())
                        .build())
                .collect(Collectors.toList());
    }

    @SneakyThrows
    public List<AirportDTO> findAll() {
        return airportRepository.findAll().stream()
                .map(AirportEntity -> AirportDTO.builder()
                        .id(AirportEntity.getId())
                        .name(AirportEntity.getName())
                        .shortName(AirportEntity.getShortName())
                        .city(AirportEntity.getCity())
                        .country(AirportEntity.getCountry())
                        .build())
                .collect(Collectors.toList());
    }

    @SneakyThrows
    public AirportDTO save(AirportDTO airportDTO) {
        AirportEntity airport = AirportEntity.builder()
                .name(airportDTO.getName())
                .shortName(airportDTO.getShortName())
                .city(airportDTO.getCity())
                .country(airportDTO.getCountry())
                .build();

        airport = airportRepository.save(airport);

        return AirportDTO.builder()
                .id(airport.getId())
                .name(airport.getName())
                .shortName(airport.getShortName())
                .city(airport.getCity())
                .country(airport.getCountry())
                .build();
    }

    public AirportDTO update(Long id, AirportDTO airportDTO) throws EntityNotFoundException{
        AirportEntity airport = airportRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("Airport is not found" + id));

        airport.setName(airportDTO.getName());
        airport.setShortName(airportDTO.getShortName());
        airport.setCity(airportDTO.getCity());
        airport.setCountry(airportDTO.getCountry());

        AirportEntity updatedAirport = airportRepository.save(airport);

        return AirportDTO.builder()
                .id(updatedAirport.getId())
                .name(updatedAirport.getName())
                .shortName(updatedAirport.getShortName())
                .city(updatedAirport.getCity())
                .country(updatedAirport.getCountry())
                .build();

    }


    @SneakyThrows
    public void deleteById(Long id) {
        airportRepository.deleteById(id);
    }

    @SneakyThrows
    public boolean existsById(Long id) {
        return airportRepository.existsById(id);
    }
}
