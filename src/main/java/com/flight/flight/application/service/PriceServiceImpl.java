package com.flight.flight.application.service;

import com.flight.flight.application.dto.PriceDTO;
import com.flight.flight.domain.entity.CabinClass.CabinClassType;
import com.flight.flight.domain.entity.PriceEntity;
import com.flight.flight.domain.repository.PriceRepository;
import com.flight.flight.domain.service.PriceService;
import jakarta.persistence.EntityNotFoundException;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class PriceServiceImpl implements PriceService {
    @Autowired
    private PriceRepository priceRepository;


    @SneakyThrows
    public Optional<PriceDTO> findById(Long id) {

        return priceRepository.findById(id)
                .map(price -> PriceDTO.builder()
                        .id(price.getId())
                        .cabinClass(price.getCabinClass())
                        .minMultiplier(price.getMinMultiplier())
                        .maxMultiplier(price.getMaxMultiplier())
                        .build());
    }

    @SneakyThrows
    public List<PriceDTO> findByCabinClass(CabinClassType cabinClass) {
        List<PriceEntity> price = priceRepository.findByCabinClass(cabinClass);

        return price.stream()
                .map(PriceEntity -> PriceDTO.builder()
                        .id(PriceEntity.getId())
                        .cabinClass(PriceEntity.getCabinClass())
                        .minMultiplier(PriceEntity.getMinMultiplier())
                        .maxMultiplier(PriceEntity.getMaxMultiplier())
                        .build())
                .collect(Collectors.toList());
    }

    @SneakyThrows
    public List<PriceDTO> findByMinMultiplier(Double minMultiplier) {
        List<PriceEntity> price = priceRepository.findByMinMultiplier(minMultiplier);

        return price.stream()
                .map(PriceEntity -> PriceDTO.builder()
                        .id(PriceEntity.getId())
                        .cabinClass(PriceEntity.getCabinClass())
                        .minMultiplier(PriceEntity.getMinMultiplier())
                        .maxMultiplier(PriceEntity.getMaxMultiplier())
                        .build())
                .collect(Collectors.toList());
    }

    @SneakyThrows
    public List<PriceDTO> findByMaxMultiplier(Double maxMultiplier) {
        List<PriceEntity> price = priceRepository.findByMaxMultiplier(maxMultiplier);

        return price.stream()
                .map(PriceEntity -> PriceDTO.builder()
                        .id(PriceEntity.getId())
                        .cabinClass(PriceEntity.getCabinClass())
                        .minMultiplier(PriceEntity.getMinMultiplier())
                        .maxMultiplier(PriceEntity.getMaxMultiplier())
                        .build())
                .collect(Collectors.toList());
    }

    @SneakyThrows
    public List<PriceDTO> findAll() {
        return priceRepository.findAll().stream()
                .map(PriceEntity -> PriceDTO.builder()
                        .id(PriceEntity.getId())
                        .cabinClass(PriceEntity.getCabinClass())
                        .minMultiplier(PriceEntity.getMinMultiplier())
                        .maxMultiplier(PriceEntity.getMaxMultiplier())
                        .build())
                .collect(Collectors.toList());
    }

    @SneakyThrows
    public PriceDTO save(PriceDTO priceDTO) {
        PriceEntity price = PriceEntity.builder()
                .cabinClass(priceDTO.getCabinClass())
                .minMultiplier(priceDTO.getMinMultiplier())
                .maxMultiplier(priceDTO.getMaxMultiplier())
                .build();

        price = priceRepository.save(price);

        return PriceDTO.builder()
                .id(price.getId())
                .cabinClass(price.getCabinClass())
                .minMultiplier(price.getMinMultiplier())
                .maxMultiplier(price.getMaxMultiplier())
                .build();
    }

    public PriceDTO update(Long id, PriceDTO priceDTO) throws EntityNotFoundException{
        PriceEntity price = priceRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("Price is not found" + id));

        price.setCabinClass(priceDTO.getCabinClass());
        price.setMinMultiplier(priceDTO.getMinMultiplier());
        price.setMaxMultiplier(priceDTO.getMaxMultiplier());

        PriceEntity updatedPrice = priceRepository.save(price);

        return PriceDTO.builder()
                .id(updatedPrice.getId())
                .cabinClass(updatedPrice.getCabinClass())
                .minMultiplier(updatedPrice.getMinMultiplier())
                .maxMultiplier(updatedPrice.getMaxMultiplier())
                .build();

    }

    @SneakyThrows
    public void deleteById(Long id) {
        priceRepository.deleteById(id);
    }

    @SneakyThrows
    public boolean existsById(Long id) {
        return priceRepository.existsById(id);
    }
}
