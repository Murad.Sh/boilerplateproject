package com.flight.flight.application.dto;
import lombok.*;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
public class PlaneDTO {
    Long id;
    String name;
    Integer numberOfSeats;
    String nameOfCompany;
}
