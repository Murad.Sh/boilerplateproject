package com.flight.flight.application.dto;

import lombok.Builder;
import lombok.Data;
import lombok.Getter;

@Data
@Builder
@Getter
public class TokenDTO {
    String accessToken;
}
