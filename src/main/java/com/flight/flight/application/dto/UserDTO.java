package com.flight.flight.application.dto;

import lombok.*;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Data
public class UserDTO {
    Long id;
    String name;
    String surname;
    String password;
    String email;
}
