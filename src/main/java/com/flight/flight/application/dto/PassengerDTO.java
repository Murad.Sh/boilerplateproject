package com.flight.flight.application.dto;

import lombok.*;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
public class PassengerDTO {
    Long id;
    String name;
    String surname;
    String email;
    String country;
    String passportNumber;
}
