package com.flight.flight.application.dto;

import com.flight.flight.domain.entity.*;
import lombok.*;
import java.util.Date;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
public class FlightDTO {
    Long id;
    AirportEntity departureAirport;
    AirportEntity arrivalAirport;
    Date departureDate;
    Date arrivalDate;
    Integer distance;
    Integer flightTime;
    PlaneEntity planeId;
    PriceEntity priceId;
}
