package com.flight.flight.application.dto;

import com.flight.flight.domain.entity.FlightEntity;
import com.flight.flight.domain.entity.PassengerEntity;
import com.flight.flight.domain.entity.TransactionEntity;
import lombok.*;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
public class TicketDTO {
    Long id;
    TransactionEntity transaction;
    FlightEntity flight;
    PassengerEntity passenger;
}
