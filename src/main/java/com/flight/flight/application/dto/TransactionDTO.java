package com.flight.flight.application.dto;
import com.flight.flight.domain.entity.Status.Status;
import lombok.*;
import java.util.Date;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
public class TransactionDTO {
    Long id;
    String rrn;
    String invoiceCode;
    Double price;
    Date paymentDate;
    Status status;
    Date createdAt;
    Date updatedAt;
    Integer flightId;
    Integer passengerId;
}
