package com.flight.flight.application.dto;

import com.flight.flight.domain.entity.CabinClass.CabinClassType;
import lombok.*;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
public class PriceDTO {
    Long id;
    CabinClassType cabinClass;
    Double minMultiplier;
    Double maxMultiplier;
}
