package com.flight.flight.domain.service;

import com.flight.flight.application.dto.PlaneDTO;
import java.util.List;
import java.util.Optional;

public interface PlaneService {
    Optional<PlaneDTO> findById(Long id);
    List<PlaneDTO> findByName(String name);

    List<PlaneDTO> findByNumberOfSeats( Integer numberOfSeats);

    List<PlaneDTO> findByNameOfCompany( String nameOfCompany);

    PlaneDTO save(PlaneDTO plane);

    PlaneDTO update(Long id, PlaneDTO plane);

    List<PlaneDTO> findAll();

    void deleteById(Long id);

    boolean existsById(Long id);
}
