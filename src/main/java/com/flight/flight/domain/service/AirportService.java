package com.flight.flight.domain.service;

import com.flight.flight.application.dto.AirportDTO;

import java.util.List;
import java.util.Optional;

public interface AirportService {
    Optional<AirportDTO> findById(Long id);
    List<AirportDTO> findByName( String name);

    List<AirportDTO> findByShortName( String shortName);

    List<AirportDTO> findByCity( String city);

    List<AirportDTO> findByCountry( String country);

    AirportDTO save(AirportDTO airport);

    AirportDTO update(Long id, AirportDTO airport);

    List<AirportDTO> findAll();

    void deleteById(Long id);

    boolean existsById(Long id);
}
