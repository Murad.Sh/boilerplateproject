package com.flight.flight.domain.service;

import com.flight.flight.application.dto.TokenDTO;
import com.flight.flight.presentation.presenter.auth.SignInRequest;
import com.flight.flight.presentation.presenter.auth.SignUpRequest;

public interface AuthService {
    TokenDTO signIn(SignInRequest signInRequest);

    TokenDTO signUp(SignUpRequest signUpRequest);
}
