package com.flight.flight.domain.service;

import com.flight.flight.application.dto.PriceDTO;
import com.flight.flight.domain.entity.CabinClass.CabinClassType;
import java.util.List;
import java.util.Optional;

public interface PriceService {
    Optional<PriceDTO> findById(Long id);
    List<PriceDTO> findByCabinClass(CabinClassType cabinClass);

    List<PriceDTO> findByMinMultiplier(Double minMultiplier);

    List<PriceDTO> findByMaxMultiplier(Double maxMultiplier);

    PriceDTO save(PriceDTO price);

    PriceDTO update(Long id, PriceDTO price);

    List<PriceDTO> findAll();

    void deleteById(Long id);

    boolean existsById(Long id);
}
