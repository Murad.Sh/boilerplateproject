package com.flight.flight.domain.repository;

import com.flight.flight.domain.entity.AirportEntity;

import java.util.List;
import java.util.Optional;

public interface AirportRepository {
    Optional<AirportEntity> findById(Long id);

    List<AirportEntity> findByName(String name);

    List<AirportEntity> findByShortName(String shortName);

    List<AirportEntity> findByCity(String city);

    List<AirportEntity> findByCountry(String country);

    AirportEntity save(AirportEntity airport);

    List<AirportEntity> findAll();

    void deleteById(Long id);

    boolean existsById(Long id);

}
