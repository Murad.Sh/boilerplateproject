package com.flight.flight.domain.repository;

import com.flight.flight.domain.entity.token.Token;
import java.util.Optional;

public interface TokenRepository {

    Optional<Token> findByToken(String token);

    Token save(Token token);

    void delete(Token token);

    void deleteAll();
}
