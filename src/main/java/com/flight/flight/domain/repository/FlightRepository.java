package com.flight.flight.domain.repository;

import com.flight.flight.domain.entity.AirportEntity;
import com.flight.flight.domain.entity.FlightEntity;
import com.flight.flight.domain.entity.PlaneEntity;
import com.flight.flight.domain.entity.PriceEntity;
import java.util.Date;
import java.util.List;
import java.util.Optional;

public interface FlightRepository {
    Optional<FlightEntity> findById(Long id);

    List<FlightEntity> findByDepartureAirport(AirportEntity departureAirport);

    List<FlightEntity> findByArrivalAirport(AirportEntity arrivalAirport);

    List<FlightEntity> findByDepartureDate(Date departureDate);

    List<FlightEntity> findByArrivalDate(Date departureDate);

    List<FlightEntity> findByDistance(Integer distance);

    List<FlightEntity> findByFlightTime(Integer flightTime);

    List<FlightEntity> findByPlaneId(PlaneEntity planeId);

    List<FlightEntity> findByPriceId(PriceEntity priceId);

    FlightEntity save(FlightEntity flight);

    List<FlightEntity> findAll();

    void deleteById(Long id);

    boolean existsById(Long id);
}
