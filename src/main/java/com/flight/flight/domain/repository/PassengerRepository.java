package com.flight.flight.domain.repository;

import com.flight.flight.domain.entity.AirportEntity;
import com.flight.flight.domain.entity.PassengerEntity;
import java.util.List;
import java.util.Optional;

public interface PassengerRepository {
    Optional<PassengerEntity> findById(Long id);

    List<PassengerEntity> findByName(String name);

    List<PassengerEntity> findBySurname(String surname);

    List<PassengerEntity> findByEmail(String email);

    List<PassengerEntity> findByCountry(String country);

    List<PassengerEntity> findByPassportNumber(String passportNumber);

    PassengerEntity save(PassengerEntity passenger);

    List<PassengerEntity> findAll();

    void deleteById(Long id);

    boolean existsById(Long id);
}
