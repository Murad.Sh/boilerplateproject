package com.flight.flight.domain.repository;

import com.flight.flight.domain.entity.user.User;
import jakarta.validation.constraints.NotBlank;
import java.util.Optional;

public interface UserRepository {
    Optional<User> findByEmail(@NotBlank String email);

    User save(User user);

    Boolean existsByEmail(@NotBlank String email);
}