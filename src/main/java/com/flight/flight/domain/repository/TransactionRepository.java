package com.flight.flight.domain.repository;

import com.flight.flight.domain.entity.PlaneEntity;
import com.flight.flight.domain.entity.Status.Status;
import com.flight.flight.domain.entity.TransactionEntity;
import java.util.Date;
import java.util.List;
import java.util.Optional;

public interface TransactionRepository {
    Optional<TransactionEntity> findById(Long id);

    List<TransactionEntity> findByRrn(String rrn);

    List<TransactionEntity> findByInvoiceCode(String invoiceCode);

    List<TransactionEntity> findByPrice(Double price);

    List<TransactionEntity> findByPaymentDate(Date paymentDate);

    List<TransactionEntity> findByStatus(Status status);

    List<TransactionEntity> findByCreatedAt(Date createdAt);

    List<TransactionEntity> findByUpdatedAt(Date updatedAt);

    List<TransactionEntity> findByFlightId(Integer flightId);

    List<TransactionEntity> findByPassengerId(Integer passengerId);

    TransactionEntity save(TransactionEntity transaction);

    List<TransactionEntity> findAll();

    void deleteById(Long id);

    boolean existsById(Long id);
}
