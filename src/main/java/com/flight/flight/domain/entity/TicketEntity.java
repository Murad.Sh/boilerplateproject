package com.flight.flight.domain.entity;

import jakarta.persistence.*;
import lombok.*;
import lombok.experimental.FieldDefaults;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
@Entity
@Table(name = "Tickets")
public class TicketEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    @OneToOne
    @JoinColumn(name = "transactions_id")
    TransactionEntity transaction;

    @OneToOne
    @JoinColumn(name = "flights_id")
    FlightEntity flight;

    @OneToOne
    @JoinColumn(name = "passengers_id")
    PassengerEntity passenger;

}
