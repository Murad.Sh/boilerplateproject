package com.flight.flight.domain.entity.user;

public enum Role {
    ADMIN,
    USER
}
