package com.flight.flight.domain.entity;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotBlank;
import lombok.*;
import lombok.experimental.FieldDefaults;
import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
@Entity
@Table(name = "Passengers")
public class PassengerEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    @Column
    @NotBlank
    String name;

    @Column
    @NotBlank
    String surname;

    @Column
    @NotBlank
    String email;

    @Column
    @NotBlank
    String country;

    @Column
    @NotBlank
    String passportNumber;

    @ManyToMany
    @JoinTable(
            name = "passengers_flight",
            joinColumns = @JoinColumn(name = "passengers_id"),
            inverseJoinColumns = @JoinColumn(name = "flights_id")
    )
    List<FlightEntity> flights = new ArrayList<>();

}
