package com.flight.flight.domain.entity;

import com.flight.flight.domain.entity.Status.Status;
import jakarta.persistence.*;
import lombok.*;
import lombok.experimental.FieldDefaults;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
@Entity
@Table(name = "Transactions")
public class TransactionEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    @Column
    String rrn;

    @Column
    String invoiceCode;

    @Column
    Double price;

    @Column
    Date paymentDate;

    @Column
    Status status;

    @Column
    Date createdAt;

    @Column
    Date updatedAt;

    @Column
    Integer flightId;

    @Column
    Integer passengerId;

}
