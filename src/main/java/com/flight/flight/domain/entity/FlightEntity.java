package com.flight.flight.domain.entity;
import jakarta.persistence.*;
import lombok.*;
import lombok.experimental.FieldDefaults;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
@Entity
@Table(name = "Flights")
public class FlightEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    @ManyToOne
    @JoinColumn(name = "departureAirport")
    AirportEntity departureAirport;

    @ManyToOne
    @JoinColumn(name = "arrivalAirport")
    AirportEntity arrivalAirport;

    @Column
    Date departureDate;

    @Column
    Date arrivalDate;

    @Column
    Integer distance;

    @Column
    Integer flightTime;

    @OneToOne
    @JoinColumn(name = "planes_id")
    PlaneEntity planeId;

    @OneToOne
    @JoinColumn(name = "prices_id")
    PriceEntity priceId;

}
