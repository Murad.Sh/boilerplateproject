package com.flight.flight.domain.entity.Status;

public enum Status {
    PENDING,
    SUCCESS,
    FAILED,
    CANCELLED;
}
