package com.flight.flight.presentation.controller;

import com.flight.flight.application.dto.TransactionDTO;
import com.flight.flight.application.service.TransactionServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/transactions")
public class TransactionController {
    @Autowired
    private TransactionServiceImpl transactionService;

    public TransactionController(TransactionServiceImpl transactionService) {

        this.transactionService = transactionService;
    }

    //Find All Transactions Method
    @GetMapping
    public ResponseEntity<List<TransactionDTO>> findAllTransactions() {
        List<TransactionDTO> transactions = transactionService.findAll();
        return ResponseEntity.ok(transactions);
    }

    // Find Transaction By ID Method
    // TODO: if user is owner
    @GetMapping("/{id}")
    public ResponseEntity<TransactionDTO> findTransactionById(@PathVariable Long id) {
        Optional<TransactionDTO> transaction = transactionService.findById(id);
        if (transaction.isPresent()) {
            return ResponseEntity.ok(transaction.get());
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    //Create New Transaction Method
//    @PostMapping
//    public ResponseEntity<TransactionDTO> createNewTransaction(@RequestBody TransactionDTO transactionDTO) {
//        TransactionDTO savedTransaction = transactionService.save(transactionDTO);
//        return new ResponseEntity<>(savedTransaction, HttpStatus.CREATED);
//    }

    //Update Transaction By ID Method
//    @PutMapping("/{id}")
//    public ResponseEntity<TransactionDTO> updateTransaction(@PathVariable Long id, @RequestBody TransactionDTO transactionDTO) {
//        TransactionDTO updatedTransaction = transactionService.update(id, transactionDTO);
//        return ResponseEntity.ok(updatedTransaction);
//    }


    //Delete Transaction By ID Method
//    @DeleteMapping("/{id}")
//    public ResponseEntity<Void> deleteTransaction(@PathVariable Long id) {
//        transactionService.deleteById(id);
//        return ResponseEntity.noContent().build();
//    }
}
