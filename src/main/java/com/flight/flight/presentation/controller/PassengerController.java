package com.flight.flight.presentation.controller;

import com.flight.flight.application.dto.PassengerDTO;
import com.flight.flight.application.service.PassengerServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/passengers")
public class PassengerController {
    @Autowired
    private PassengerServiceImpl passengerService;

    public PassengerController(PassengerServiceImpl passengerService) {

        this.passengerService = passengerService;
    }

    //Find All Passengers Method
    @GetMapping
    public ResponseEntity<List<PassengerDTO>> findAllPassengers() {
        List<PassengerDTO> passengers = passengerService.findAll();
        return ResponseEntity.ok(passengers);
    }

    //Find Passenger By ID Method
    @GetMapping("/{id}")
    public ResponseEntity<PassengerDTO> findPassengerById(@PathVariable Long id) {
        Optional<PassengerDTO> passenger = passengerService.findById(id);
        if (passenger.isPresent()) {
            return ResponseEntity.ok(passenger.get());
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    //Create New Passenger Method
    @PostMapping
    public ResponseEntity<PassengerDTO> createNewPassenger(@RequestBody PassengerDTO passengerDTO) {
        PassengerDTO savedPassenger = passengerService.save(passengerDTO);
        return new ResponseEntity<>(savedPassenger, HttpStatus.CREATED);
    }

    //Update Passenger By ID Method
    @PutMapping("/{id}")
    public ResponseEntity<PassengerDTO> updatePassenger(@PathVariable Long id, @RequestBody PassengerDTO passengerDTO) {
        PassengerDTO updatedPassenger = passengerService.update(id, passengerDTO);
        return ResponseEntity.ok(updatedPassenger);
    }


    //Delete Passenger By ID Method
    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deletePassenger(@PathVariable Long id) {
        passengerService.deleteById(id);
        return ResponseEntity.noContent().build();
    }
}
