package com.flight.flight.presentation.controller;

import com.flight.flight.domain.service.AuthService;
import com.flight.flight.presentation.presenter.auth.AuthenticationResponse;
import com.flight.flight.presentation.presenter.auth.SignInRequest;
import com.flight.flight.presentation.presenter.auth.SignUpRequest;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/auth")
public class AuthController {
    @Autowired
    private AuthService authService;

    @RequestMapping(path = "/sign-in", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> signIn(@Valid @RequestBody SignInRequest request) {
        var token = authService.signIn(request);
        return ResponseEntity.ok().body(AuthenticationResponse.builder().accessToken(token.getAccessToken()).build());
    }

    @RequestMapping(path = "/sign-up", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> signUp(@Valid @RequestBody SignUpRequest request) {
        var token = authService.signUp(request);
        return ResponseEntity.accepted().body(AuthenticationResponse.builder().accessToken(token.getAccessToken()).build());
    }
}
