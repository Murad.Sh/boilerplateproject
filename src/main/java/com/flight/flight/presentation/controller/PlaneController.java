package com.flight.flight.presentation.controller;

import com.flight.flight.application.dto.PlaneDTO;
import com.flight.flight.application.service.PlaneServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/planes")
public class PlaneController {
    @Autowired
    private PlaneServiceImpl planeService;

    public PlaneController(PlaneServiceImpl planeService) {

        this.planeService = planeService;
    }

    //Find All Planes Method
    @GetMapping
    public ResponseEntity<List<PlaneDTO>> findAllPlanes() {
        List<PlaneDTO> planes = planeService.findAll();
        return ResponseEntity.ok(planes);
    }

    //Find Plane By ID Method
    @GetMapping("/{id}")
    public ResponseEntity<PlaneDTO> findPlaneById(@PathVariable Long id) {
        Optional<PlaneDTO> plane = planeService.findById(id);
        if (plane.isPresent()) {
            return ResponseEntity.ok(plane.get());
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    //Create New Plane Method
    @PostMapping
    public ResponseEntity<PlaneDTO> createNewPlane(@RequestBody PlaneDTO planeDTO) {
        PlaneDTO savedPlane = planeService.save(planeDTO);
        return new ResponseEntity<>(savedPlane, HttpStatus.CREATED);
    }

    //Update Plane By ID Method
    @PutMapping("/{id}")
    public ResponseEntity<PlaneDTO> updatePlane(@PathVariable Long id, @RequestBody PlaneDTO planeDTO) {
        PlaneDTO updatedPlane = planeService.update(id, planeDTO);
        return ResponseEntity.ok(updatedPlane);
    }


    //Delete Plane By ID Method
    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deletePlane(@PathVariable Long id) {
        planeService.deleteById(id);
        return ResponseEntity.noContent().build();
    }

}
