package com.flight.flight.presentation.controller;

import com.flight.flight.application.dto.FlightDTO;
import com.flight.flight.application.service.FlightServiceImpl;
import com.flight.flight.presentation.presenter.flight.CreateFlightRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/flights")
public class FlightController {

    @Autowired
    private FlightServiceImpl flightService;

    public FlightController(FlightServiceImpl flightService) {

        this.flightService = flightService;
    }

    //Find All Flights Method
    @GetMapping
    public ResponseEntity<List<FlightDTO>> findAllFlights() {
        List<FlightDTO> flights = flightService.findAll();
        return ResponseEntity.ok(flights);
    }

    //Find Flight By ID Method
    @GetMapping("/{id}")
    public ResponseEntity<FlightDTO> findFlightById(@PathVariable Long id) {
        Optional<FlightDTO> flight = flightService.findById(id);
        if (flight.isPresent()) {
            return ResponseEntity.ok(flight.get());
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    //Create New Flight Method
    @PostMapping
    public ResponseEntity<FlightDTO> createNewFlight(@RequestBody CreateFlightRequest createFlightRequest) {
        FlightDTO savedFlight = flightService.save(createFlightRequest);
        return new ResponseEntity<>(savedFlight, HttpStatus.CREATED);
    }

    //Update Flight By ID Method
    @PutMapping("/{id}")
    public ResponseEntity<FlightDTO> updateFlight(@PathVariable Long id, @RequestBody FlightDTO flightDTO) {
        FlightDTO updatedFlight = flightService.update(id, flightDTO);
        return ResponseEntity.ok(updatedFlight);
    }


    //Delete Flight By ID Method
    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteFlight(@PathVariable Long id) {
        flightService.deleteById(id);
        return ResponseEntity.noContent().build();
    }

    // New methods
    /*@GetMapping("/departure/{airport}")
    public ResponseEntity<List<FlightDTO>> findFlightsByDepartureAirport(@PathVariable AirportEntity departureAirport) {
        List<FlightDTO> flights = flightService.findByDepartureAirport(departureAirport);
        return ResponseEntity.ok(flights);
    }

    @GetMapping("/arrival/{airport}")
    public ResponseEntity<List<FlightDTO>> findFlightsByArrivalAirport(@PathVariable AirportEntity arrivalAirport) {
        List<FlightDTO> flights = flightService.findByArrivalAirport(arrivalAirport);
        return ResponseEntity.ok(flights);
    }*/
}
