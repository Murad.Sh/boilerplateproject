package com.flight.flight.presentation.presenter.auth;

import jakarta.validation.constraints.NotEmpty;
import lombok.Data;

@Data
public class SignInRequest {
    @NotEmpty
    String email;
    @NotEmpty
    String password;
}
