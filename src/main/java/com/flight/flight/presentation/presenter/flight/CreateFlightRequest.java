package com.flight.flight.presentation.presenter.flight;

import lombok.Data;
import lombok.NonNull;

import java.util.Date;

@Data
public class CreateFlightRequest {
    @NonNull
    Integer distance;
    @NonNull
    Integer flightTime;
    @NonNull Date departureDate;
    @NonNull Date arrivalDate;
    @NonNull Long arrivalAirportId;
    @NonNull Long departureAirportId;
    @NonNull Long planeId;
    @NonNull Long priceId;
}
